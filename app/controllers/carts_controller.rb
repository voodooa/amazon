class CartsController < ApplicationController
  before_action :require_user
  before_action :set_cart

  def show
  end

  def add_item
    product = Product.find(params[:product_id])
    if product.stock < params[:quantity].to_i
      redirect_to product_path(product), alert: "Not enough stock available."
      return
    end

    @cart_item = @cart.cart_items.find_or_initialize_by(product: product)
    @cart_item.quantity ||= 0
    @cart_item.quantity += params[:quantity].to_i

    if @cart_item.save
      redirect_to cart_path, notice: "#{product.name} was added to your cart."
    else
      redirect_to product_path(product), alert: "Could not add product to cart."
    end
  end

  def update_item
    product = Product.find(params[:product_id])
    if product.stock < params[:quantity].to_i
      redirect_to cart_path, alert: "Not enough stock available."
      return
    end

    @cart_item = @cart.cart_items.find_by(product: product)
    @cart_item.quantity = params[:quantity].to_i

    if @cart_item.save
      redirect_to cart_path, notice: "Cart item updated."
    else
      redirect_to cart_path, alert: "Could not update cart item."
    end
  end

  def remove_item
    product = Product.find(params[:product_id])
    @cart_item = @cart.cart_items.find_by(product: product)

    if @cart_item.destroy
      redirect_to cart_path, notice: "Product removed from cart."
    else
      redirect_to cart_path, alert: "Could not remove product from cart."
    end
  end

  def checkout
    ActiveRecord::Base.transaction do
      order = current_user.buyer.orders.create!(total_price: 0)
      @cart.cart_items.each do |cart_item|
        order.order_items.create!(
          product: cart_item.product,
          quantity: cart_item.quantity,
          total_price: cart_item.product.price * cart_item.quantity
        )
      end
      @cart.cart_items.destroy_all
      order.save!
    end

    redirect_to orders_path, notice: "Order successfully created."
  end

  private

  def set_cart
    @cart = current_user.cart || current_user.create_cart
  end
end
