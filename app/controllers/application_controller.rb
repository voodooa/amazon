class ApplicationController < ActionController::Base
  helper_method :current_user, :logged_in?

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def logged_in?
    !!current_user
  end

  def require_user
    unless logged_in?
      flash[:alert] = "You must be logged in to perform that action."
      redirect_to login_path
    end
  end

  def require_buyer
    unless logged_in? && current_user.buyer
      flash[:alert] = "You must be a buyer to access this section."
      redirect_to root_path
    end
  end

  def require_seller
    unless logged_in? && current_user.seller
      flash[:alert] = "You must be a seller to access this section."
      redirect_to root_path
    end
  end
end
