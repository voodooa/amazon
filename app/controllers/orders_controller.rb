class OrdersController < ApplicationController
  before_action :require_user
  before_action :require_buyer, only: %i[new create]

  def index
    @orders = current_user.buyer.orders.includes(:order_items)
  end

  def show
    @order = Order.includes(:order_items).find(params[:id])
  end

  def new
    @order = current_user.buyer.orders.build
  end

  def create
    @order = current_user.buyer.orders.build(order_params)
    if @order.save
      @order.order_items.create!(
        product_id: order_params[:product_id],
        quantity: order_params[:quantity],
        total_price: order_params[:quantity] * Product.find(order_params[:product_id]).price
      )
      redirect_to @order, notice: "Order was successfully created."
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @order = Order.find(params[:id])
  end

  def update
    @order = Order.find(params[:id])
    if @order.update(order_params)
      redirect_to @order, notice: "Order was successfully updated."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @order = Order.find(params[:id])
    @order.destroy
    redirect_to orders_url, notice: "Order was successfully destroyed."
  end

  private

  def order_params
    params.require(:order).permit(:product_id, :quantity)
  end
end
