# app/controllers/users_controller.rb
class UsersController < ApplicationController
    before_action :require_user, only: [:index, :show]

    def index
      @users = User.all
    end

    def show
      @user = User.find(params[:id])
    end
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.create_seller if params[:user][:seller] == "1"
      session[:user_id] = @user.id
      redirect_to root_path, notice: 'Account created successfully.'
    else
      render :new
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :seller)
  end
end
