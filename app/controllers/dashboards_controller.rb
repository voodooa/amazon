class DashboardsController < ApplicationController
  before_action :require_user

  def index
    @orders = current_user.buyer.orders.includes(order_items: :product)
  end
end
