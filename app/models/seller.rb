# app/models/seller.rb
class Seller < ApplicationRecord
  belongs_to :user
  has_many :products
end
