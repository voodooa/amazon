# app/models/buyer.rb
class Buyer < ApplicationRecord
  belongs_to :user
  has_many :orders
end
