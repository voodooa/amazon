class Order < ApplicationRecord
  belongs_to :buyer
  has_many :order_items, dependent: :destroy

  validates :total_price, presence: true, numericality: { greater_than_or_equal_to: 0 }

  before_save :set_total_price

  def set_total_price
    self.total_price = order_items.sum('quantity * total_price')
  end
end
