class User < ApplicationRecord
  has_secure_password

  has_one :buyer, dependent: :destroy
  has_one :seller, dependent: :destroy, required: false
  has_many :orders, through: :buyer
  has_one :cart, dependent: :destroy

  validates :email, presence: true, uniqueness: true, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :password, presence: true, length: { minimum: 6 }, confirmation: true

  after_create :create_buyer_account_and_cart

  private

  def create_buyer_account_and_cart
    create_buyer
    create_cart
  end
end
