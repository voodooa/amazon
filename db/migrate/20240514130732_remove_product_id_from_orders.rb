class RemoveProductIdFromOrders < ActiveRecord::Migration[7.1]
  def change
    remove_reference :orders, :product, foreign_key: true
  end
end
