# Pin npm packages by running ./bin/importmap

pin "application"
pin_all_from "app/assets/stylesheets"
pin_all_from "app/views"



pin "@hotwired/turbo-rails", to: "turbo.min.js"
# config/importmap.rb
pin "application", preload: true
pin "@hotwired/turbo-rails", to: "https://cdn.jsdelivr.net/npm/@hotwired/turbo@7.1.0/dist/turbo.min.js", preload: true
