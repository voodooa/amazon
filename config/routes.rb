Rails.application.routes.draw do
  get 'dashboards/index'
  get 'carts/show'
  get 'carts/add_item'
  get 'carts/update_item'
  get 'carts/remove_item'
  get 'carts/checkout'
  root 'products#index'

  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  get '/register', to: 'users#new'
  post '/register', to: 'users#create'

  resources :orders
  resources :products
  resources :sellers
  resources :buyers
  resources :users

  resource :cart, only: [:show] do
    post 'add/:product_id', to: 'carts#add_item', as: 'add_item'
    patch 'update_item/:product_id', to: 'carts#update_item', as: 'update_item'
    delete 'remove_item/:product_id', to: 'carts#remove_item', as: 'remove_item'
  end

  post '/checkout', to: 'carts#checkout'
  get "/dashboard", to: "dashboards#index"
end
